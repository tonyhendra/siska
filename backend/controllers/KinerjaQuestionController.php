<?php

namespace backend\controllers;

use Yii;
use backend\models\KinerjaQuestion;
use backend\models\KinerjaQuestionSearch;
use backend\models\KinerjaOption;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\DynamicForms;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;


/**
 * KinerjaQuestionController implements the CRUD actions for KinerjaQuestion model.
 */
class KinerjaQuestionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all KinerjaQuestion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KinerjaQuestionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KinerjaQuestion model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new KinerjaQuestion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new KinerjaQuestion();
        $modelOption = [new KinerjaOption];

        if ($model->load(Yii::$app->request->post())) {

            $modelOption = DynamicForms::createMultiple(KinerjaOption::className());
            DynamicForms::loadMultiple($modelOption, Yii::$app->request->post());

            $valid = $model->validate();
            $valid = DynamicForms::validateMultiple($modelOption) && $valid;

            // $data=Yii::$app->request->post('Orders');
            // $this->simpan($data,$model->id_order);
            //
            //if($valid){
                $transaction = \Yii::$app->db->beginTransaction();
                try{
                    if($flag = $model->save(false)){

                        foreach ($modelOption as $val) {
                            # code...
                            $val->id_question = $model->id_question;
                            //$harga = $val->getHarga($val->id_product);
                            //$total_harga = $total_harga+$harga;
                            if(!$flag = $val->save(false)){
                                $transaction->rollBack();
                                break;
                            }
                        }
                        

                    }
                    if($flag){
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id_question]);
                    }
                }catch(Exception $e){
                    $transaction->rollBack();
                }
            //}
            // else{

            //     return $this->render('create', [
            //         'model' => $model, 
            //         'modelOption' => (empty($modelOption)) ? [new modelOption] : $modelOption,
            //         'error' => $valid,
            //     ]
            //     );

            // }
            
        }
        else{

            return $this->render('create', [
                'model' => $model, 
                'modelOption' => (empty($modelOption)) ? [new modelOption] : $modelOption,
                'error' => 0,
            ]);
            //return $this->redirect(['test']);

        }

    }

    /**
     * Updates an existing KinerjaQuestion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelOption = $model->kinerjaOption;

        if ($model->load(Yii::$app->request->post())) {
            $oldIDs = ArrayHelper::map($modelOption, 'id', 'id');
            $modelOption = DynamicForms::createMultiple(KinerjaOption::classname(), $modelOption);
            DynamicForms::loadMultiple($modelOption, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelOption, 'id', 'id')));
            
            $valid = $model->validate();
            $valid = DynamicForms::validateMultiple($modelOption) && $valid;

            // $data=Yii::$app->request->post('Orders');
            // $this->simpan($data,$model->id_order);

            if($valid){
                $transaction = \Yii::$app->db->beginTransaction();
                try{
                    if($flag = $model->save(false)){
                        if(!empty($deletedIDs)){
                            KinerjaOption::deleteAll(['id'=> $deletedIDs]);
                        }

                        foreach ($modelOption as $val) {
                            # code...
                            $val->id_question = $model->id_question;
                            //$harga = $val->getHarga($val->id_product);
                            //$total_harga = $total_harga+$harga;
                            if(!$flag = $val->save(false)){
                                $transaction->rollBack();
                                break;
                            }
                        }
                        

                    }
                    if($flag){
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id_question]);
                    }
                }catch(Exception $e){
                    $transaction->rollBack();
                }

            }
        }

        return $this->render('update', [
            'model' => $model, 'modelOption' => $modelOption
        ]);

    }

    /**
     * Deletes an existing KinerjaQuestion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the KinerjaQuestion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return KinerjaQuestion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = KinerjaQuestion::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
