<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "kinerja_option".
 *
 * @property int $id
 * @property int $id_question
 * @property string $option_text
 * @property int $kriteria
 */
class KinerjaOption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kinerja_option';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_question', 'option_text', 'kriteria'], 'required'],
            [['id_question', 'kriteria'], 'integer'],
            [['option_text'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_question' => 'Id Question',
            'option_text' => 'Option Text',
            'kriteria' => 'Kriteria',
        ];
    }
}
