<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "kinerja_question".
 *
 * @property int $id_question
 * @property string $question_text
 * @property double $bobot
 */
class KinerjaQuestion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kinerja_question';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question_text', 'bobot'], 'required'],
            [['bobot'], 'number'],
            [['question_text'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_question' => 'Id Question',
            'question_text' => 'Question Text',
            'bobot' => 'Bobot',
        ];
    }

    public function getKinerjaOption(){
        return $this->hasMany(KinerjaOption::className(), ['id_question' => 'id_question']);
    }

     public function setKinerjaOption($value)
    {
        $this->loadRelated('KinerjaOption', $value);
    }

}
