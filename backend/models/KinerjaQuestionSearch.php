<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\KinerjaQuestion;

/**
 * KinerjaQuestionSearch represents the model behind the search form of `app\models\KinerjaQuestion`.
 */
class KinerjaQuestionSearch extends KinerjaQuestion
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_question'], 'integer'],
            [['question_text'], 'safe'],
            [['bobot'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KinerjaQuestion::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_question' => $this->id_question,
            'bobot' => $this->bobot,
        ]);

        $query->andFilterWhere(['like', 'question_text', $this->question_text]);

        return $dataProvider;
    }
}
