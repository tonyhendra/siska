<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "periode".
 *
 * @property int $id
 * @property int $periode
 * @property int $tahun
 */
class Periode extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'periode';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['periode', 'tahun'], 'required'],
            [['periode', 'tahun'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'periode' => 'Periode',
            'tahun' => 'Tahun',
        ];
    }
}
