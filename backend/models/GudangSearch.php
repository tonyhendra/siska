<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Gudang;

/**
 * GudangSearch represents the model behind the search form of `app\models\Gudang`.
 */
class GudangSearch extends Gudang
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['plant', 'deskripsi_gudang', 'lokasi'], 'safe'],
            [['id_pengelola'], 'integer'],
            [['lat', 'lng'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Gudang::find();

        // add conditions that should always apply here
        $query->joinWith(['pengelola']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_pengelola' => $this->id_pengelola,
            'lat' => $this->lat,
            'lng' => $this->lng,
        ]);

        $query->andFilterWhere(['like', 'plant', $this->plant])
            ->andFilterWhere(['like', 'deskripsi_gudang', $this->deskripsi_gudang])
            ->andFilterWhere(['like', 'lokasi', $this->lokasi]);

        return $dataProvider;
    }
}
