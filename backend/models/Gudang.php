<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "gudang".
 *
 * @property string $plant
 * @property string $deskripsi_gudang
 * @property int $id_pengelola
 * @property string $lokasi
 * @property string $lat
 * @property string $lng
 */
class Gudang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gudang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['plant', 'deskripsi_gudang', 'id_pengelola', 'lokasi', 'lat', 'lng'], 'required'],
            [['id_pengelola'], 'integer'],
            [['lat', 'lng'], 'number'],
            [['plant'], 'string', 'max' => 4],
            [['deskripsi_gudang'], 'string', 'max' => 50],
            [['lokasi'], 'string', 'max' => 255],
            [['plant'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'plant' => 'Plant',
            'deskripsi_gudang' => 'Deskripsi Gudang',
            'id_pengelola' => 'Id Pengelola',
            'lokasi' => 'Lokasi',
            'lat' => 'Lat',
            'lng' => 'Lng',
        ];
    }

    public function getPengelola(){
        return $this->hasOne(PengelolaGudang::className(), ['id_pengelola' => 'id_pengelola']);
    }
}
