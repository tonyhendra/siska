<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "pengelola_gudang".
 *
 * @property int $id_pengelola
 * @property string $deskripsi_pengelola
 */
class PengelolaGudang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengelola_gudang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['deskripsi_pengelola'], 'required'],
            [['deskripsi_pengelola'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pengelola' => 'Id Pengelola',
            'deskripsi_pengelola' => 'Deskripsi Pengelola',
        ];
    }
}
