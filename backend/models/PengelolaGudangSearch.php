<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\PengelolaGudang;

/**
 * PengelolaGudangSearch represents the model behind the search form of `app\models\PengelolaGudang`.
 */
class PengelolaGudangSearch extends PengelolaGudang
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pengelola'], 'integer'],
            [['deskripsi_pengelola'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PengelolaGudang::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_pengelola' => $this->id_pengelola,
        ]);

        $query->andFilterWhere(['like', 'deskripsi_pengelola', $this->deskripsi_pengelola]);

        return $dataProvider;
    }
}
