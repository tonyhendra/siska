<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\KinerjaQuestion */

$this->title = $model->id_question;
$this->params['breadcrumbs'][] = ['label' => 'Kinerja Questions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kinerja-question-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_question], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_question], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_question',
            'question_text',
            'bobot',
        ],
    ]) ?>

</div>
