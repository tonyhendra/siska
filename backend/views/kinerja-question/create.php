<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KinerjaQuestion */

$this->title = 'Create Kinerja Question';
$this->params['breadcrumbs'][] = ['label' => 'Kinerja Questions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kinerja-question-create">

	<?php print_r($error); ?>
    <?= $this->render('_form', [
        'model' => $model, 'modelOption' => $modelOption
    ]) ?>

</div>
