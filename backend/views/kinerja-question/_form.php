<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\KinerjaQuestion */
/* @var $form yii\widgets\ActiveForm */

$js='jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {
        jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {
            jQuery(this).html("Option: " + (index + 1))
        });
    });

    jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {
        jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {
            jQuery(this).html("Option: " + (index + 1))
        });
    });
    ';

$this->registerJs($js);
?>

<div class="kinerja-question-form">
    <section class="content">
    <div class="row">
	<div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Add Periode</h3>
            </div>
            <div class="box-body">
	              <!-- Date -->
	              <div class="form-group">


				    <?php $form = ActiveForm::begin(['id' => 'option-form']); ?>

				    <?= $form->field($model, 'question_text')->textInput(['maxlength' => true]) ?>

				    <?= $form->field($model, 'bobot')->textInput() ?>

		            <?php DynamicFormWidget::begin([
		                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
		                'widgetBody' => '.container-items', // required: css class selector
		                'widgetItem' => '.item', // required: css class
		                'limit' => 5, // the maximum times, an element can be cloned (default 999)
		                'min' => 0, // 0 or 1 (default 1)
		                'insertButton' => '.add-item', // css class
		                'deleteButton' => '.remove-item', // css class
		                'model' => $modelOption[0],
		                'formId' => 'option-form',
		                'formFields' => [
		                    'id_product',
		                    'jumlah',
		                ],
		            ]); ?>
		            <div class="panel panel-default">
		                <div class="panel-heading">
		                    <i class="fa fa-envelope"></i> Options
		                    <button type="button" class="pull-right add-item btn btn-success btn-xs"><i class="fa fa-plus"></i> Add Option</button>
		                    <div class="clearfix"></div>
		                </div>
		                <div class="panel-body container-items"><!-- widgetContainer -->
		                    <?php foreach ($modelOption as $index => $modelOption): ?>
		                        <div class="item panel panel-default"><!-- widgetBody -->
		                            <div class="panel-heading">
		                                <span class="panel-title-address">Option: <?= ($index + 1) ?></span>
		                                <button type="button" class="pull-right remove-item btn btn-danger btn-xs"><i class="fa fa-minus"></i></button>
		                                <div class="clearfix"></div>
		                            </div>
		                            <div class="panel-body">
		                                <?php
		                                    // necessary for update action.
		                                    if (!$modelOption->isNewRecord) {
		                                        echo Html::activeHiddenInput($modelOption, "[{$index}]id");
		                                    }
		                                ?>
		                                <?= $form->field($modelOption, "[{$index}]option_text")->textInput(['maxlength' => true])  ?>
		                                <?= $form->field($modelOption, "[{$index}]kriteria")->textInput(['maxlength' => true]) ?>
		                            </div>
		                        </div>
		                    <?php endforeach; ?>
		                </div>
		            </div>
		            <?php DynamicFormWidget::end(); ?>

				    <div class="form-group">
				        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
				    </div>

				    <?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>
	</div>
	</section>

</div>
