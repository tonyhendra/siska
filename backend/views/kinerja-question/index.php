<?php

use yii\helpers\Html;
use yii\grid\GridView;
use fedemotta\datatables\DataTables;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KinerjaQuestionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kinerja Questions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kinerja-question-index">
        
    <div class="box">
        <div class="box-header">
            <p>
                <?= Html::a('Create Kinerja Question', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
        </div>

        <div class="box-body">

            <?= DataTables::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id_question',
                    'question_text',
                    'bobot',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>
