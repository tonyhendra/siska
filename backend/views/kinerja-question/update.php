<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KinerjaQuestion */

$this->title = 'Update Kinerja Question: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Kinerja Questions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_question, 'url' => ['view', 'id' => $model->id_question]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kinerja-question-update">

    <?= $this->render('_form', [
        'model' => $model,'modelOption' => $modelOption
    ]) ?>

</div>
