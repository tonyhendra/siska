<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Periode */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Periodes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="periode-view">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </div>
            <div class="box-body">
              <!-- Date -->
            <div class="form-group">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'periode',
                        'tahun',
                    ],
                ]) ?>
            </div>
        </div>
    </div>


</div>
