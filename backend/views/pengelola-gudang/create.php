<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PengelolaGudang */

$this->title = 'Create Pengelola Gudang';
$this->params['breadcrumbs'][] = ['label' => 'Pengelola Gudangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengelola-gudang-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
