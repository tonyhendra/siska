<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PengelolaGudang */

$this->title = 'Update Pengelola Gudang: '.$model->deskripsi_pengelola;
$this->params['breadcrumbs'][] = ['label' => 'Pengelola Gudangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_pengelola, 'url' => ['view', 'id' => $model->id_pengelola]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pengelola-gudang-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
