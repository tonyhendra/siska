<?php

use yii\helpers\Html;
use yii\grid\GridView;
use fedemotta\datatables\DataTables;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PengelolaGudangSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pengelola Gudangs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengelola-gudang-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="box">
        <div class="box-header">
        <p>
            <?= Html::a('Create Pengelola Gudang', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        </div>

        <div class="box-body">

        <?= DataTables::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id_pengelola',
                'deskripsi_pengelola',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        </div>
    </div>

</div>
