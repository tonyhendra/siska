<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PengelolaGudang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengelola-gudang-form">
	<div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Add Pengelola Gudang</h3>
            </div>
            <div class="box-body">
              <!-- Date -->
              <div class="form-group">

			    <?php $form = ActiveForm::begin(); ?>

			    <?= $form->field($model, 'deskripsi_pengelola')->textInput(['maxlength' => true]) ?>

			    <div class="form-group">
			        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
			    </div>

			    <?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>
</div>
