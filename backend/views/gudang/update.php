<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Gudang */

$this->title = 'Update Gudang: '.$model->deskripsi_gudang;
$this->params['breadcrumbs'][] = ['label' => 'Gudangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->plant, 'url' => ['view', 'id' => $model->plant]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="gudang-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
