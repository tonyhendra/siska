<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\models\PengelolaGudang;
use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\services\DirectionsWayPoint;
use dosamigos\google\maps\services\TravelMode;
use dosamigos\google\maps\overlays\PolylineOptions;
use dosamigos\google\maps\services\DirectionsRenderer;
use dosamigos\google\maps\services\DirectionsService;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\overlays\Marker;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\services\DirectionsRequest;
use dosamigos\google\maps\overlays\Polygon;
use dosamigos\google\maps\layers\BicyclingLayer;
use yii\web\JsExpression;


/* @var $this yii\web\View */
/* @var $model app\models\Gudang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gudang-form">
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                  <div class="box box-primary">
                    <div class="box-header">
                      <h3 class="box-title">Add Gudang</h3>
                    </div>
                    <div class="box-body">
                      <!-- Date -->
                        <div class="form-group">

                            <?php $form = ActiveForm::begin(); ?>

                            <?= $form->field($model, 'plant')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'deskripsi_gudang')->textInput(['maxlength' => true]) ?>
                            <?= $form->field($model, 'id_pengelola')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(PengelolaGudang::find()->all(), 'id_pengelola', 'deskripsi_pengelola'),
                                'options' => ['placeholder' => 'Select Pengelola ...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                            ?>

                            <?= $form->field($model, 'lokasi')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'lat')->textInput(['readOnly'=> true]) ?>

                            <?= $form->field($model, 'lng')->textInput(['readOnly'=> true]) ?>
                            <?php
                                echo '<h3>Lokasi</h3>';
                                echo "<div id='address'></div>";

                                $model->isNewRecord ? $coord = new LatLng(['lat' => -7.24917, 'lng' => 112.75083]) : $coord = new LatLng(['lat' => $model->lat, 'lng' => $model->lng]);

                                //$coord = new LatLng(['lat' => 39.720089311812094, 'lng' => 2.91165944519042]);
                                $map = new Map([
                                    'center' => $coord,
                                    'zoom' => 14,
                                    'width' => '100%',
                                ]);
                                $map->appendScript('
                                    if(document.getElementById(\'gudang-lat\').value != ""){
                                         var myMarker = new google.maps.Marker({
                                            position: new google.maps.LatLng(document.getElementById(\'gudang-lat\').value, document.getElementById(\'gudang-lng\').value),
                                            draggable: true
                                        });
                                    } else{
                                        var myMarker = new google.maps.Marker({
                                            position: new google.maps.LatLng(-7.24917, 112.75083),
                                            draggable: true
                                        });
                                    }
                                    
                                    gmap0.setCenter(myMarker.position);
                                    myMarker.setMap(gmap0);

                                    google.maps.event.addListener(myMarker, \'dragend\', function(evt){
                                        document.getElementById(\'gudang-lat\').value = evt.latLng.lat();
                                        document.getElementById(\'gudang-lng\').value = evt.latLng.lng();
                                        getReverseGeocodingData(evt.latLng.lat(),evt.latLng.lng());

                                    });

                                    function getReverseGeocodingData(lat, lng) {
                                        var latlng = new google.maps.LatLng(lat, lng);
                                        // This is making the Geocode request
                                        var geocoder = new google.maps.Geocoder();
                                        geocoder.geocode({ latLng: latlng }, function (results, status) {
                                            if (status !== google.maps.GeocoderStatus.OK) {
                                                alert(status);
                                            }
                                            // This is checking to see if the Geoeode Status is OK before proceeding
                                            if (status == google.maps.GeocoderStatus.OK) {
                                                console.log(results);
                                                var address = (results[0].formatted_address);
                                                document.getElementById(\'address\').innerHTML = address;
                                            }
                                        });
                                    }
                                ');
                                
                                //$map->addOverlay($marker);
                                 
                                // Display the map -finally :)
                                //ho "center : ".$map->getCenter();
                                echo $map->display();
                            ?>


                            <div class="form-group">
                                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                            </div>

                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
            </div>
    </div>
</div>
</section>

</div>
