<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Gudang */

$this->title = $model->plant;
$this->params['breadcrumbs'][] = ['label' => 'Gudangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gudang-view">

     <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header">
                <?= Html::a('Update', ['update', 'id' => $model->plant], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->plant], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </div>
            <div class="box-body">
              <!-- Date -->
            <div class="form-group">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'plant',
                        'deskripsi_gudang',
                        ['label' => 'Pengelola',
                        'value' => function($data) { return $data->pengelola->deskripsi_pengelola; }],
                        'lokasi',
                        'lat',
                        'lng',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>


</div>
