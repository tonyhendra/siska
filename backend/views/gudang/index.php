<?php

use yii\helpers\Html;
use yii\grid\GridView;
use fedemotta\datatables\DataTables;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GudangSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gudangs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gudang-index">

    
    <div class="box">
        <div class="box-header">
        <p>
            <?= Html::a('Create Gudang', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        </div>

        <div class="box-body">

            <?= DataTables::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'plant',
                    'deskripsi_gudang',
                    // ['label' => 'Pengelola',
                    // 'value' => function($data){ return $data->pengelola->deskripsi_pengelola; }
                    // ],
                    'id_pengelola',
                    //'lokasi',
                    //'lat',
                    //'lng',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>

</div>
