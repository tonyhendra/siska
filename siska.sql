-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 13, 2018 at 09:14 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `siska`
--

-- --------------------------------------------------------

--
-- Table structure for table `gudang`
--

CREATE TABLE `gudang` (
  `plant` varchar(4) NOT NULL,
  `deskripsi_gudang` varchar(50) NOT NULL,
  `id_pengelola` int(11) DEFAULT NULL,
  `lokasi` varchar(255) DEFAULT NULL,
  `lat` decimal(10,8) DEFAULT NULL,
  `lng` decimal(11,8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gudang`
--

INSERT INTO `gudang` (`plant`, `deskripsi_gudang`, `id_pengelola`, `lokasi`, `lat`, `lng`) VALUES
('B201', 'DC Medan I', 1, 'null', '0.00000000', '0.00000000'),
('B202', 'DC Medan II', 2, 'null', '0.00000000', '0.00000000'),
('B203', 'Padang I', 3, 'null', '0.00000000', '0.00000000'),
('B204', 'Padang II', 1, 'null', '0.00000000', '0.00000000'),
('B205', 'DC Lampung I', 3, 'null', '0.00000000', '0.00000000'),
('B206', 'DC Lampung II', 1, 'null', '0.00000000', '0.00000000'),
('B208', 'DC Makassar II', 3, 'null', '0.00000000', '0.00000000'),
('B214', 'DC Makassar I', 1, 'null', '0.00000000', '0.00000000'),
('B216', 'DC Makassar III', 4, 'null', '0.00000000', '0.00000000'),
('B401', 'Kotacane', 1, 'null', '0.00000000', '0.00000000'),
('B402', 'Lhokseumawe', 5, 'null', '0.00000000', '0.00000000'),
('B403', 'Blang Pidie', 1, 'nn', '0.00000000', '0.00000000'),
('B404', 'Aceh Tamiang (Langsa)', 5, 'nn', '0.00000000', '0.00000000'),
('B405', 'Sibolga', 2, 'nn', '0.00000000', '0.00000000'),
('B406', 'Balige', 2, 'nn', '0.00000000', '0.00000000'),
('B407', 'Asahan', 2, 'nn', '0.00000000', '0.00000000'),
('B408', 'Simalungun', 2, 'nn', '0.00000000', '0.00000000'),
('B409', 'Dairi', 1, 'nn', '0.00000000', '0.00000000'),
('B410', 'Tanah Karo', 2, 'nn', '0.00000000', '0.00000000'),
('B411', 'Serdang Bedagai', 2, 'nn', '0.00000000', '0.00000000'),
('B412', 'Labuhanbatu-Sumut', 0, '', '0.00000000', '0.00000000'),
('B413', 'Padang Sidempuan', 0, '', '0.00000000', '0.00000000'),
('B414', 'Painan Pesisir Selatan', 0, '', '0.00000000', '0.00000000'),
('B415', 'Solok', 0, '', '0.00000000', '0.00000000'),
('B416', 'Batusangkar', 0, '', '0.00000000', '0.00000000'),
('B417', 'Pasaman Barat', 0, '', '0.00000000', '0.00000000'),
('B418', 'Bukit Tinggi', 0, '', '0.00000000', '0.00000000'),
('B419', 'Payakumbuh', 0, '', '0.00000000', '0.00000000'),
('B420', 'IndraGiri Hulu', 0, '', '0.00000000', '0.00000000'),
('B421', 'Pekanbaru', 0, '', '0.00000000', '0.00000000'),
('B422', 'Dumai', 0, '', '0.00000000', '0.00000000'),
('B423', 'Tanjung Pinang', 0, '', '0.00000000', '0.00000000'),
('B424', 'Kerinci', 0, '', '0.00000000', '0.00000000'),
('B425', 'Jambi', 0, '', '0.00000000', '0.00000000'),
('B426', 'Merangin', 0, '', '0.00000000', '0.00000000'),
('B427', 'Lahat', 0, '', '0.00000000', '0.00000000'),
('B428', 'Martapura', 0, '', '0.00000000', '0.00000000'),
('B429', 'Palembang', 0, '', '0.00000000', '0.00000000'),
('B431', 'Lubuk Linggau', 0, '', '0.00000000', '0.00000000'),
('B432', 'Bengkulu II', 0, '', '0.00000000', '0.00000000'),
('B433', 'Bengkulu III', 0, '', '0.00000000', '0.00000000'),
('B434', 'Pringsewu-Lampung', 0, '', '0.00000000', '0.00000000'),
('B435', 'Bandar Jaya', 0, '', '0.00000000', '0.00000000'),
('B436', 'Gunung Sugih', 0, '', '0.00000000', '0.00000000'),
('B437', 'Kotabumi', 0, '', '0.00000000', '0.00000000'),
('B438', 'Belitung', 0, '', '0.00000000', '0.00000000'),
('B439', 'Pangkal Pinang', 0, '', '0.00000000', '0.00000000'),
('B440', 'Lombok  Lembar  II', 0, '', '0.00000000', '0.00000000'),
('B441', 'Lombok  Pringgabaya', 0, '', '0.00000000', '0.00000000'),
('B442', 'Lombok Lembar-NTB', 0, '', '0.00000000', '0.00000000'),
('B443', 'Sumbawa', 0, '', '0.00000000', '0.00000000'),
('B444', 'Sumabawa II-NTB', 0, '', '0.00000000', '0.00000000'),
('B445', 'Dompu', 0, '', '0.00000000', '0.00000000'),
('B446', 'Bima', 0, '', '0.00000000', '0.00000000'),
('B447', 'Kupang', 0, '', '0.00000000', '0.00000000'),
('B448', 'Ende-NTT', 0, '', '0.00000000', '0.00000000'),
('B449', 'Labuan Bajo II', 0, '', '0.00000000', '0.00000000'),
('B450', 'Labuan Bajo I', 0, '', '0.00000000', '0.00000000'),
('B451', 'Reo Flores (Manggarai)', 0, '', '0.00000000', '0.00000000'),
('B452', 'Timor Tengah Utara (Atambua)', 0, '', '0.00000000', '0.00000000'),
('B455', 'Ketapang II', 0, '', '0.00000000', '0.00000000'),
('B456', 'Sekadau', 0, '', '0.00000000', '0.00000000'),
('B457', 'Pontianak I', 0, '', '0.00000000', '0.00000000'),
('B459', 'Sambas', 0, '', '0.00000000', '0.00000000'),
('B460', 'Kumai', 0, '', '0.00000000', '0.00000000'),
('B461', 'Sampit II-Kalteng', 0, '', '0.00000000', '0.00000000'),
('B463', 'Kapuas', 0, '', '0.00000000', '0.00000000'),
('B464', 'Pagatan - Tanah Bumbu-Kalsel', 0, '', '0.00000000', '0.00000000'),
('B465', 'Tapin', 0, '', '0.00000000', '0.00000000'),
('B466', 'Plaihari (Tanah laut)', 0, '', '0.00000000', '0.00000000'),
('B467', 'Barabai', 0, '', '0.00000000', '0.00000000'),
('B468', 'Banjarmasin I-Kalsel', 0, '', '0.00000000', '0.00000000'),
('B470', 'Tanjung/Tabalong', 0, '', '0.00000000', '0.00000000'),
('B471', 'Balik Papan', 0, '', '0.00000000', '0.00000000'),
('B472', 'Samarinda I', 0, '', '0.00000000', '0.00000000'),
('B473', 'Samarinda II', 0, '', '0.00000000', '0.00000000'),
('B474', 'Bolmong', 0, '', '0.00000000', '0.00000000'),
('B475', 'Bitung I', 0, '', '0.00000000', '0.00000000'),
('B476', 'Bitung II', 0, '', '0.00000000', '0.00000000'),
('B477', 'Luwuk Banggai', 0, '', '0.00000000', '0.00000000'),
('B478', 'Morowali', 0, '', '0.00000000', '0.00000000'),
('B479', 'Toli-Toli', 0, '', '0.00000000', '0.00000000'),
('B480', 'Palu I', 0, '', '0.00000000', '0.00000000'),
('B481', 'Palu II', 0, '', '0.00000000', '0.00000000'),
('B482', 'Bulukumba', 0, '', '0.00000000', '0.00000000'),
('B483', 'Takalar', 0, '', '0.00000000', '0.00000000'),
('B484', 'Barru', 0, '', '0.00000000', '0.00000000'),
('B485', 'Bone I', 0, '', '0.00000000', '0.00000000'),
('B486', 'Bone II', 0, '', '0.00000000', '0.00000000'),
('B487', 'Bone III', 0, '', '0.00000000', '0.00000000'),
('B488', 'Wajo', 0, '', '0.00000000', '0.00000000'),
('B489', 'Luwu Timur', 0, '', '0.00000000', '0.00000000'),
('B490', 'Pare - Pare', 0, '', '0.00000000', '0.00000000'),
('B491', 'Palopo', 0, '', '0.00000000', '0.00000000'),
('B492', 'Kendari I', 0, '', '0.00000000', '0.00000000'),
('B494', 'Kendari III', 0, '', '0.00000000', '0.00000000'),
('B495', 'Bau-bau', 0, '', '0.00000000', '0.00000000'),
('B496', 'Gorontalo', 0, '', '0.00000000', '0.00000000'),
('B497', 'Polman', 1, 'Jl Teuku Umar, Kuajang, Binuang, Kabupaten Polewali Mandar, Sulawesi Barat 91312, Indonesia', '-3.44676256', '119.36809659'),
('B498', 'Mamuju', 0, '', '0.00000000', '0.00000000'),
('B499', 'Maluku Tengah (Kobisonta)', 0, '', '0.00000000', '0.00000000'),
('B4A0', 'Namlea (Buru)', 0, '', '0.00000000', '0.00000000'),
('B4A1', 'Tobelo-Maluku', 0, '', '0.00000000', '0.00000000'),
('B4A2', 'Halmahera Timur', 0, '', '0.00000000', '0.00000000'),
('B4A3', 'Ternate-Maluku', 0, '', '0.00000000', '0.00000000'),
('B4A4', 'Sorong-Papua', 0, '', '0.00000000', '0.00000000'),
('B4A7', 'Merauke II', 0, '', '0.00000000', '0.00000000'),
('B4A8', 'Merauke III', 0, '', '0.00000000', '0.00000000'),
('B4AA', 'Keerom', 0, '', '0.00000000', '0.00000000'),
('B4AB', 'Nabire', 0, '', '0.00000000', '0.00000000'),
('B4AC', 'Waikabubak (Sumba Barat)', 0, '', '0.00000000', '0.00000000'),
('B4AD', 'Waingapu-Sumba Timur-NTT', 0, '', '0.00000000', '0.00000000'),
('B4AE', 'Lombok Tengah-NTB', 0, '', '0.00000000', '0.00000000'),
('B4AF', 'Lembar Lombok (Serumbung)-NTB', 0, '', '0.00000000', '0.00000000'),
('B4AG', 'Jeneponto', 0, '', '0.00000000', '0.00000000'),
('B4AH', 'Luwu Utara', 0, '', '0.00000000', '0.00000000'),
('B4AI', 'Tarakan-Kaltara', 0, '', '0.00000000', '0.00000000'),
('B4AJ', 'Pidie', 0, '', '0.00000000', '0.00000000'),
('B4AK', 'Nias', 0, '', '0.00000000', '0.00000000'),
('B4AL', 'Padang Pariaman', 0, '', '0.00000000', '0.00000000'),
('B4AM', 'Dharmasraya', 0, '', '0.00000000', '0.00000000'),
('B4AN', 'Siak-Sumbar', 0, '', '0.00000000', '0.00000000'),
('B4AO', 'Tanggamus', 0, '', '0.00000000', '0.00000000'),
('B4AP', 'Mesuji', 0, '', '0.00000000', '0.00000000'),
('B4AQ', 'Banjarmasin III-Kalsel', 0, '', '0.00000000', '0.00000000'),
('B4AR', 'Bulungan', 0, '', '0.00000000', '0.00000000'),
('B4AS', 'Melaboh', 0, '', '0.00000000', '0.00000000'),
('B4AT', 'Subulus Salam', 0, '', '0.00000000', '0.00000000'),
('B4AU', 'Pasaman', 0, '', '0.00000000', '0.00000000'),
('B4AW', 'Paser (Tanah Grogot)', 0, '', '0.00000000', '0.00000000'),
('B4AX', 'Kolaka', 0, '', '0.00000000', '0.00000000'),
('B4AY', 'Soppeng', 0, '', '0.00000000', '0.00000000'),
('B4AZ', 'Pahuwato', 0, '', '0.00000000', '0.00000000'),
('B4B0', 'Minahasa Selatan', 0, '', '0.00000000', '0.00000000'),
('B4BA', 'Lombok Timur', 0, '', '0.00000000', '0.00000000'),
('B4BB', 'Manokwari-Papua', 0, '', '0.00000000', '0.00000000'),
('B4BC', 'Pontianak III GSG', 0, '', '0.00000000', '0.00000000'),
('B4BD', 'Solok Selatan', 0, '', '0.00000000', '0.00000000'),
('B4BE', 'Tulang Bawang Barat', 0, '', '0.00000000', '0.00000000'),
('B4BF', 'Lampung Barat', 0, '', '0.00000000', '0.00000000'),
('B4BG', 'Batola', 0, '', '0.00000000', '0.00000000'),
('B4BH', 'Palangkaraya', 0, '', '0.00000000', '0.00000000'),
('B4BI', 'Panajam Paser Utara', 0, '', '0.00000000', '0.00000000'),
('B4BJ', 'Kutai Timur (Sangata)', 0, '', '0.00000000', '0.00000000'),
('B4BL', 'Gorontalo Utara', 0, '', '0.00000000', '0.00000000'),
('B4BM', 'Boalemo', 0, '', '0.00000000', '0.00000000'),
('B4BN', 'Kolaka Utara', 0, '', '0.00000000', '0.00000000'),
('B4BO', 'Luwu', 0, '', '0.00000000', '0.00000000'),
('B4BP', 'Sidrap', 6, 'Jl. Pare-Pare - Sidrap No.22, Uluale, Watang Pulu, Kabupaten Sidenreng Rappang, Sulawesi Selatan 91661, Indonesia', '-3.90813635', '119.74928677'),
('B4BQ', 'Sinjai', 0, '', '0.00000000', '0.00000000'),
('B4BR', 'Biak Numfor', 0, '', '0.00000000', '0.00000000'),
('B4BS', 'Padang Lawas', 0, '', '0.00000000', '0.00000000'),
('B4BT', 'Timika', 0, '', '0.00000000', '0.00000000'),
('B4BU', 'Wajo II', 0, '', '0.00000000', '0.00000000'),
('B4BV', 'Tapanuli Utara', 0, '', '0.00000000', '0.00000000'),
('B4BW', 'Bengkulu I', 5, 'null', '0.00000000', '0.00000000'),
('B4BX', 'Lombok Utara', 0, '', '0.00000000', '0.00000000'),
('B4BY', 'Sumbawa Barat', 0, '', '0.00000000', '0.00000000');

-- --------------------------------------------------------

--
-- Table structure for table `kinerja`
--

CREATE TABLE `kinerja` (
  `id_kinerja` int(11) NOT NULL,
  `plant` int(11) NOT NULL,
  `id_pengelola` int(11) NOT NULL,
  `kepala_gudang` varchar(50) NOT NULL,
  `petugas` varchar(50) NOT NULL,
  `total_score` double NOT NULL,
  `kategori` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `id_periode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kinerja_option`
--

CREATE TABLE `kinerja_option` (
  `id` int(11) NOT NULL,
  `id_question` int(11) NOT NULL,
  `option_text` varchar(250) NOT NULL,
  `kriteria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kinerja_option`
--

INSERT INTO `kinerja_option` (`id`, `id_question`, `option_text`, `kriteria`) VALUES
(1, 4, 'Tepat Waktu', 10);

-- --------------------------------------------------------

--
-- Table structure for table `kinerja_question`
--

CREATE TABLE `kinerja_question` (
  `id_question` int(11) NOT NULL,
  `question_text` varchar(250) NOT NULL,
  `bobot` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kinerja_question`
--

INSERT INTO `kinerja_question` (`id_question`, `question_text`, `bobot`) VALUES
(4, 'Pengiriman Laporan Harian', 0.1);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1520410302),
('m130524_201442_init', 1520410306);

-- --------------------------------------------------------

--
-- Table structure for table `pengelola_gudang`
--

CREATE TABLE `pengelola_gudang` (
  `id_pengelola` int(11) NOT NULL,
  `deskripsi_pengelola` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengelola_gudang`
--

INSERT INTO `pengelola_gudang` (`id_pengelola`, `deskripsi_pengelola`) VALUES
(1, 'Bhanda Ghara Reksa'),
(2, 'Gresik Cipta Sejahtera'),
(3, 'Petrokopindo Cipta Selaras'),
(4, 'Surya Buana Sentosa'),
(5, 'Varuna Tirta Perkasa'),
(6, 'Aneka Jasa Grhadika'),
(7, 'Duta Niaga Sejati');

-- --------------------------------------------------------

--
-- Table structure for table `periode`
--

CREATE TABLE `periode` (
  `id` int(11) NOT NULL,
  `periode` int(11) NOT NULL,
  `tahun` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `periode`
--

INSERT INTO `periode` (`id`, `periode`, `tahun`) VALUES
(1, 1, 2018);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'tonyhendra', 'NAi_ESHmJF_JEObQLdoaUNjvkcTud2tp', '$2y$13$XWv9kJ2YTc41VKWJKF6mjumA8EchmDtlNIjo7bszC1xFgi.EIKsc.', NULL, 'tonyhendra0@gmail.com', 10, 1520410324, 1520410324);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gudang`
--
ALTER TABLE `gudang`
  ADD PRIMARY KEY (`plant`);

--
-- Indexes for table `kinerja`
--
ALTER TABLE `kinerja`
  ADD PRIMARY KEY (`id_kinerja`);

--
-- Indexes for table `kinerja_option`
--
ALTER TABLE `kinerja_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kinerja_question`
--
ALTER TABLE `kinerja_question`
  ADD PRIMARY KEY (`id_question`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `pengelola_gudang`
--
ALTER TABLE `pengelola_gudang`
  ADD PRIMARY KEY (`id_pengelola`);

--
-- Indexes for table `periode`
--
ALTER TABLE `periode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kinerja`
--
ALTER TABLE `kinerja`
  MODIFY `id_kinerja` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kinerja_option`
--
ALTER TABLE `kinerja_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `kinerja_question`
--
ALTER TABLE `kinerja_question`
  MODIFY `id_question` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `pengelola_gudang`
--
ALTER TABLE `pengelola_gudang`
  MODIFY `id_pengelola` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `periode`
--
ALTER TABLE `periode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
